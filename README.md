GuessNum
========

- 系统会自动生成一个4位且每一位都各不相同的数字。
- 当你猜测该数字时有A和B两种提示信息，其中A代表所猜数字中有该数字且位置也正确而B代表所猜数字仅有该数字但是位置不正确。
- 当猜对全部数字时即闯关成功（即4A0B）。


 ![效果图4](https://github.com/GinSmile/GuessNum/blob/master/image/result.png?raw=true)