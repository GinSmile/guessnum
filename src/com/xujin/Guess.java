//本程序是文曲星的猜数字游戏，系统会自动生成一个4位且每一位都各不相同的数字
//当你猜测该数字时有A和B两种提示信息，其中A代表所猜数字中有该数字且位置也正确
//而B代表所猜数字仅有该数字但是位置不正确。当猜对全部数字时即闯关成功（即4A0B）

package com.xujin;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JDialog;

import com.xujin.*;
import com.xujin.MainFrame.HowToPlayDialog;

public class Guess {	
	//Scanner cin = new Scanner(System.in);
	public Guess(){		
		//初始化，防止二次游戏的时候出错
		outputResult = "";
		countSteps = 0;
		aimNum = new ArrayList<Integer>();
		guessNum = new ArrayList<Integer>();
		aCount = 0;
		bCount = 0;
		arrayRange = 10;
		
		//产生一个四位都不相同的数字
		//利用一个ArrayList，随机在其中选取一个0~9的数，选完就在ArrayList中删除，
		//然后继续在其中随机选取数字，直到4个全部选取完毕
		
		//初始化ArrayList
		ArrayList<Integer> array = new ArrayList<Integer>();
		for(int i = 0; i < 10; i++)
			array.add(i);
		Random random = new Random();
		for(int i = 0; i < 4; i++){
			int singleNumIndex = random.nextInt(arrayRange--);			
			aimNum.add(array.get(singleNumIndex)); 
			array.remove(singleNumIndex);
		}
		System.out.println(aimNum);

	}
	
	public static void processInput(){
		countSteps++;
		//int inputNum = Integer.parseInt(MainFrame.textField.getText().trim());
		for(int i = 0; i < 4; i++){
			guessNum.add(0, MainFrame.numberInput % 10);
			MainFrame.numberInput = MainFrame.numberInput / 10;
		}
		if(aimNum.equals(guessNum)){
			outputResult += "congratulations! You make it!";
			MainFrame.textArea.setText(outputResult);
			MainFrame.okButton.setVisible(false);
			
			GuessNumberMain.mainFrame.showGameOverDialog();
		}
		else{
			output();
			MainFrame.label.setText("已用步数：" + Guess.countSteps);
			guessNum.clear();
		}
	}
	

	
	private static void output(){
		for(int i = 0; i < 4; i++)
			if(aimNum.get(i) == guessNum.get(i))
				aCount++;
		
		for(int i: aimNum)
			for(int j: guessNum)
				if(i == j)
					bCount++;
		
		bCount -= aCount;		
		outputResult += (guessNum.get(0).toString()
				+ guessNum.get(1).toString()
				+ guessNum.get(2).toString()
				+ guessNum.get(3).toString()
				+ "  " + aCount + "A" + bCount + "B" + "\r\n");
		MainFrame.textArea.setText(outputResult);
		MainFrame.textField.setText("");
		aCount = 0;
		bCount = 0;
	}
	
	public static boolean uniqueNumber(String input){
		if(input.charAt(0) != input.charAt(1) &&
			input.charAt(0) != input.charAt(2) &&
			input.charAt(0) != input.charAt(3) &&
			input.charAt(1) != input.charAt(2) &&
			input.charAt(1) != input.charAt(3) &&
			input.charAt(2) != input.charAt(3))
			return true;
		return false;
	}
	
	private static int arrayRange;  //上界，用于随机产生一个数组下标
	private static ArrayList<Integer> aimNum;	
	private static ArrayList<Integer> guessNum;
	private static int aCount;
	private static int bCount;
	public static String outputResult;
	public static int countSteps;
}
