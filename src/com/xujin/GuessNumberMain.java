package com.xujin;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class GuessNumberMain {
	public static void main(String...args){
		GuessNumberMain newGame = new GuessNumberMain();
		newGame.init();
	}
	
	/************************************************初始化*****************************************************/
	public  void init(){
		mainFrame = new MainFrame();
		Guess guess = new Guess();
		MainFrame.menuBar = new JMenuBar();
		MainFrame.textArea.setText("");
		MainFrame.textField.setText("");
		MainFrame.label.setText("已用步数：" + Guess.countSteps);
		//设置点击关闭后弹出对话框
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				 int returnValue = JOptionPane.showConfirmDialog(null, "确实要退出游戏吗？", "退出游戏", JOptionPane.YES_NO_OPTION);
				    if (returnValue == JOptionPane.YES_OPTION)
				      System.exit(0);
			}
		});
		mainFrame.setResizable(false);
		mainFrame.setVisible(true);
	}
	public static MainFrame mainFrame;
}



class MainFrame extends JFrame{
	
	/************************************************初始化框架*****************************************************/
	
	public MainFrame(){
		setTitle("ButtonTest");
		Toolkit kit = Toolkit.getDefaultToolkit();
		   
		//设置默认窗口大小和位置
		Dimension screenSize = kit.getScreenSize();
		screenWidth = screenSize.width;
		screenHeight = screenSize.height;
	    this.setBounds(screenWidth / 4, screenHeight / 4, screenWidth / 3, screenHeight / 2);
	    
	    //设置程序图标和程序名
	    Image imgIcon = kit.getImage("image/icon.png");
	    setIconImage(imgIcon);       
	    setTitle("Guess Number");
	    
	    
	    
	    //设置菜单
	    setMenu();
	    setJMenuBar(menuBar);
	    
	    //添加组件
	    add(new NorthPanel(), BorderLayout.NORTH);   
	    add(new WestPanel(), BorderLayout.CENTER);   
	    add(new SouthPanel(), BorderLayout.SOUTH);
	}
	
	
	/************************************************菜单相关*****************************************************/
	
	private void setMenu(){
		   JMenu gameMenu = new JMenu("游戏");
		   menuBar.add(gameMenu);	   
		   JMenuItem newGameMenu = new JMenuItem("开始新游戏");
		   newGameMenu.setIcon(new ImageIcon("image/start.png"));
		   gameMenu.add(newGameMenu);
		   newGameMenu.addActionListener(new ActionListener(){
			   @Override
			   public void actionPerformed(ActionEvent event){
				   restart();
			   }
		   });
		   JMenuItem exitGameMenu = new JMenuItem("退出游戏");
		   exitGameMenu.setIcon(new ImageIcon("image/exit.png"));
		   gameMenu.add(exitGameMenu);
		   exitGameMenu.addActionListener(new ActionListener(){
			   @Override
			   public void actionPerformed(ActionEvent event){
				   int returnValue = JOptionPane.showConfirmDialog(null, "确实要退出游戏吗？", "退出游戏", JOptionPane.YES_NO_OPTION);
				    if (returnValue == JOptionPane.YES_OPTION)
				      System.exit(0);
			   }
		   });
		   
		   
		   JMenu setMenu = new JMenu("设置");
		   menuBar.add(setMenu);
		   setLookMenu = new JMenu("设置观感");
		   setLookMenu.setIcon(new ImageIcon("image/look.png"));
		   setMenu.add(setLookMenu);
		   //设置观感选项
		    UIManager.LookAndFeelInfo[] infos = UIManager.getInstalledLookAndFeels();
		    for(UIManager.LookAndFeelInfo info: infos){
		    	addItem(info.getName(), info.getClassName());
		    	//System.out.println(info.getClassName());
		    }
		   
		    
		   JMenu helpMenu = new JMenu("帮助");
		   menuBar.add(helpMenu);	   
		   JMenuItem aboutMeMenu = new JMenuItem("关于作者");
		   aboutMeMenu.setIcon(new ImageIcon("image/aboutMe.png"));
		   aboutMeMenu.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
					JDialog dialog = null;
					if(dialog == null)
						dialog = new AboutDialog(MainFrame.this);
					dialog.setVisible(true);
			}
		   });
		   JMenuItem howToPlayGameMenu = new JMenuItem("游戏玩法");
		   howToPlayGameMenu.setIcon(new ImageIcon("image/help.png"));
		   howToPlayGameMenu.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
						JDialog dialog = null;
						if(dialog == null)
							dialog = new HowToPlayDialog(MainFrame.this);
						dialog.setVisible(true);
				}
			   });
		   helpMenu.add(aboutMeMenu);
		   helpMenu.add(howToPlayGameMenu);
		   
		   
	   }
	
	
	
	public void addItem(String name, final String plafName){
		JMenuItem item = new JMenuItem(name);
		setLookMenu.add(item);
		
		item.addActionListener(new ActionListener(){
			@Override
	    	public void actionPerformed(ActionEvent event){
	    		try{
	    			UIManager.setLookAndFeel(plafName);
	    			SwingUtilities.updateComponentTreeUI(MainFrame.this);
	    		}
	    		catch(Exception e){
	    			e.printStackTrace();
	    		}
	    	}
		});
	}
	
	/************************************************面板设置*****************************************************/
	
	class NorthPanel extends JPanel{		
		public NorthPanel(){
			setLayout(new GridLayout(1, 2));
			add(new JLabel(" Welcome to the  \"Guess Number\"  Game !   Just enjoy it !", SwingConstants.LEFT));
		}
	}
	
	class WestPanel extends JPanel{
		public WestPanel(){
			setLayout(new GridLayout(1, 1));
			textArea= new JTextArea(12, 40);
			textArea.setLineWrap(true);
			textArea.setEditable(false);
			JScrollPane scrollPane = new JScrollPane(textArea);
			add(scrollPane);
			
		}
	}
	
	class SouthPanel extends JPanel{
		public SouthPanel(){
			setLayout(new GridLayout(1, 4));
			textField = new JTextField(40);
			add(new JLabel("输入数字：", SwingConstants.RIGHT));
			add(textField);
			okButton = new JButton("OK");
			JButton lazeButton = new JButton("OK");
			okButton.setIcon(new ImageIcon("image/ok.png"));
			okButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					String input = textField.getText().trim();
					if(input.length() == 4 && Guess.uniqueNumber(input)){
						numberInput = Integer.parseInt(input);
						Guess.processInput();
					}
					else{
						JDialog errorNotiocDialog = new ErrorNoticeDialog(MainFrame.this);
						errorNotiocDialog.setVisible(true);
					}
					
				}
			});
			add(okButton);
			add(lazeButton);
			lazeButton.setVisible(false);
			label = new JLabel();
			label.setText("已用步数：" + Guess.countSteps);
			add(label);
		}
	}
	
	
	/************************************************重新开始游戏*****************************************************/
	
	public void restart(){
		//GuessNumberMain.mainFrame.dispose();
		new GuessNumberMain().init();
	}
	
	/************************************************对话框Dialog*****************************************************/
	
	//游戏结束对话框
	public void showGameOverDialog(){
		JDialog dialog = null;
		if(dialog == null)
			dialog = new GameOverDialog(MainFrame.this);
		dialog.setVisible(true);
	}
	
	class GameOverDialog extends JDialog{
		public GameOverDialog(JFrame owner){
			super(owner, "游戏结束", true);
			add(new JLabel("<html><h2><i>恭喜你</i><h2><hr/><p>胜利！是否重新开始？</p></html>"), 
					BorderLayout.CENTER);
			JPanel panel = new JPanel();
			JButton ok = new JButton("重新开始");
			
			ok.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
					setVisible(false);
					restart();
				}
			});
			
			panel.add(ok);
			add(panel, BorderLayout.SOUTH);
			this.setBounds(screenWidth * 3 / 8, screenHeight * 3 / 8, screenWidth /5, screenHeight/6); 
		}
	}
	
	
	//输入错误提示对话框
	class ErrorNoticeDialog extends JDialog{
		public ErrorNoticeDialog(JFrame owner){
			super(owner, "输入错误", true);
			setResizable(false);
			JLabel aboutLabel = new JLabel("<html><p>错误！</p><p>请重新输入每位数<i>均不同</i>的四位数字！</p></html>");
			add(aboutLabel);
			
			JPanel panel = new JPanel();
			JButton ok = new JButton("确定");
			
			ok.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
					setVisible(false);
					textField.setText("");
				}
			});
			
			panel.add(ok);
			add(panel, BorderLayout.SOUTH);
			this.setBounds(screenWidth * 3 / 8, screenHeight * 3 / 8, screenWidth /5, screenHeight/5); 
		}
	}
	
	//关于作者对话框
	class AboutDialog extends JDialog{
		public AboutDialog(JFrame owner){
			super(owner, "关于作者", true);
			setResizable(false);
			JLabel aboutLabel = new JLabel("<html><h4><i>关于作者</i></h4>" +
					"<hr/><p>徐进</p><p>Otaku, to be...中二攻城狮</p>" +
					"<p>新浪微博：<a href='weibo.com/smilexujin'>weibo.com/smilexujin</a></p></html>");
			add(aboutLabel);
			
			aboutLabel.addMouseListener(new MouseAdapter(){ 
				@Override 
				public void mouseClicked(MouseEvent e) { 
					Desktop desktop=Desktop.getDesktop(); 
					try { 
						desktop.browse(new URI("http://weibo.com/smilexujin")); 
					} catch (IOException e1) { 
						e1.printStackTrace(); 
					} catch (URISyntaxException e1) { 
						e1.printStackTrace(); 
					}	
				}
				}); 
			
			JPanel panel = new JPanel();
			JButton ok = new JButton("OK");
			
			ok.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
					setVisible(false);
				}
			});
			
			panel.add(ok);
			add(panel, BorderLayout.SOUTH);
			
			this.setBounds(screenWidth * 3 / 8, screenHeight * 3 / 8, screenWidth /3, screenHeight/3); 
		}
	}
	
	//游戏玩法对话框
	class HowToPlayDialog extends JDialog{
		public HowToPlayDialog(JFrame owner){
			super(owner, "游戏玩法", true);
			add(new JLabel("<html><h2><i>游戏玩法</i><h2><hr/><p>猜数字游戏，系统会自动生成一个4位且每一位都各不相同的数字" +
					"当你猜测该数字时有A和B两种提示信息，其中A代表所猜数字中有该数字且位置也正确" +
					"而B代表所猜数字仅有该数字但是位置不正确。当猜对全部数字时即闯关成功（即4A0B）</p></html>"), 
					BorderLayout.CENTER);
			JPanel panel = new JPanel();
			JButton ok = new JButton("OK");
			
			ok.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
					setVisible(false);
				}
			});
			
			panel.add(ok);
			add(panel, BorderLayout.SOUTH);
			this.setBounds(screenWidth * 3 / 8, screenHeight * 3 / 8, screenWidth /3, screenHeight/4); 
		}
	}
	
	
	
	public static int screenWidth;
	public static int screenHeight;
	
	public JMenu setLookMenu;
	public static JMenuBar menuBar = new JMenuBar();
	public static JTextArea textArea;
	public static JTextField textField;
	public static int numberInput;
	public static JButton okButton;
	public static JLabel label;
}

